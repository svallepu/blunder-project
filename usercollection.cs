﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace blunder_project
{
    internal class usercollection
    {
        /// <summary>
        /// This is an implementation of a class "All users", to represent number  
        /// of users registered in an Application.
        /// </summary>

            List<usercollection> userList;


            /** This is a helper method to create a User Object.
             * @param name
             * @param age
             * @param profession
             * @param interests
             * @param hobbies
             */

            public bool createNewUser(String name, String age, String profession, String interests, String hobbies)
            {
                try
                {
                    usercollection adduser = new usercollection(name, age, profession, interests, hobbies);
                    userList.Add(adduser);
                    return true;
                }
                catch (Exception ex)
                {
                    return false;
                }

            }
        }
    }
}



