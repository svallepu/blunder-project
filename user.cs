﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace blunder_project
{
    internal class user
    {

        /// <summary>
        /// This is an implementation of a class "User", to represent a 
        /// user in my implementation of an Application
        /// </summary>

        
        
            String name;                //Representation of User name;
            String age;                 //Representation of age;
            String profession;          //Representation of profession;
            String interests;           //Representation of interests;
            String hobbies;             //Representation of hobbies;


            /** 
             * declaring a constructor for User class 
             */

            public user(string name, string age, string profession, string interests, string hobbies)
            {




                this.name = name;
                this.age = age;
                this.profession = profession;
                this.interests = interests;
                this.hobbies = hobbies;
                Console.WriteLine(this);
            }


            /**
            * This is a override of tostring method to print the User object in 
            * human readable form.
            */


            override
            public String ToString()
            {
                String stringToReturn = "-------------These are list of All Users------------- \n";

                stringToReturn += "Name:          " + this.name + "\n";
                stringToReturn += "Age:           " + this.age + "\n";
                stringToReturn += "Profession:    " + this.profession + "\n";
                stringToReturn += "Interests:     " + this.interests + "\n";
                stringToReturn += "Hobbies:       " + this.hobbies + "\n";

                return stringToReturn;

            }

        }
    }




