﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace blunder_project
{
    internal class tester
    {
        /// <summary>
        /// This is an implementation of a class "Test", which is used to test  
        /// all the classes in an Application.
        /// </summary>

        public static void Main()
        {

            /** Calling a Allusers class. */

            usercollection uc = new usercollection();
            uc.createNewUser("priya", "20", "Iam a Backend Developer", "Interacting with new people", "dancing");

            usercollection uc2 = new usercollection();
            uc2.createNewUser("siri", "21", "Iam a Tester", "Testing", "playing");

            usercollection uc3 = new usercollection();
            uc3.createNewUser("sravs", "22", "Iam an Engineer", "Dancing", "coding");

            usercollection uc4 = new usercollection();
            uc4.createNewUser("ashok", "23", "Iam a Java Developer", "coding", "community service");

            usercollection uc5 = new usercollection();
            uc5.createNewUser("anil", "22", "Iam a Java Developer", "Dancing", "playing chess");

            usercollection uc6 = new usercollection();
            uc6.createNewUser("yash", "22", "Iam a student", "Gaming", "community service");

            usercollection uc7 = new usercollection();
            uc7.createNewUser("harsha", "21", "Iam a student", "travelling", "cooking");

            usercollection uc8 = new usercollection();
            uc8.createNewUser("swapna", "21", "Iam an Engineer", "Travelling", "listening music");

            usercollection uc9 = new usercollection();
            uc9.createNewUser("jayanth", "22", "Iam a pharmacist", "Drug Inspector", "playing an instrument");

            usercollection uc10 = new usercollection();
            uc10.createNewUser("Mani", "30", "Iam a Doctor", "problem solving", "watching movies");




        }
    }

}

